//
//  LextechJSONQuizViewController.m
//  LextechJSONQuiz
//
//  Created by David Rossi on 4/15/11.
//  Copyright 2011 Lextech Global Services. All rights reserved.
//

#import "LextechJSONQuizViewController.h"
#import "JSONKit.h"

@implementation LextechJSONQuizViewController



@synthesize loadingIndicator, transactions;


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad 
{
    [super viewDidLoad];
	
	[self requestTransactionData];
}


- (IBAction)refreshButtonPressed:(id)sender
{
	[self requestTransactionData];
}


- (void)requestTransactionData
{	
	
	//connect to https://mobiletest.lextech.com/test.php and download transaction data

	
	[self.loadingIndicator startAnimating];
	
}


#pragma mark -


- (void)didReceiveMemoryWarning 
{
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload 
{
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
	
	self.transactions = nil;
	self.loadingIndicator = nil;
}


- (void)dealloc 
{
	transactions = nil;
	loadingIndicator = nil;	
	
    [super dealloc];
}

@end
