//
//  LextechJSONQuizViewController.h
//  LextechJSONQuiz
//
//  Created by David Rossi on 4/15/11.
//  Copyright 2011 Lextech Global Services. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LextechJSONQuizViewController : UIViewController
{
	UIActivityIndicatorView *loadingIndicator;
	
	NSArray *transactions;
}

@property(nonatomic, retain) IBOutlet UIActivityIndicatorView *loadingIndicator;

@property(nonatomic, retain) NSArray *transactions;


- (IBAction)refreshButtonPressed:(id)sender;
- (void)requestTransactionData;



@end

